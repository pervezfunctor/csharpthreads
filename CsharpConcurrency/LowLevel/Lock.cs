﻿using System.Threading;

namespace Seartipy
{
    public class Lock
    {
        private readonly object _lock = new object();

        private readonly Condition condition;

        public Lock()
        {
            condition = new Condition(this);
        }

        public void Acquire(ref bool lockTaken)
        {
            Monitor.Enter(_lock, ref lockTaken);
        }

        public void Release()
        {
            Monitor.Exit(_lock);
        }

        public Condition GetCondition()
        {
            return condition;
        }

        public class Condition
        {
            private readonly Lock _lock;

            public Condition(Lock _lock)
            {
                this._lock = _lock;
            }

            public void Wait()
            {
                Monitor.Wait(_lock._lock);
            }

            public void Notify()
            {
                Monitor.Pulse(_lock._lock);
            }

            public void NotifyAll()
            {
                Monitor.PulseAll(_lock._lock);
            }
        }
    }
}
