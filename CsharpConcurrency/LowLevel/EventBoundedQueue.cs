﻿using System.Threading;


namespace Seartipy
{
    //Very little testing done, might not work.
    public class EventBoundedQueue<T>
    {
        private readonly T[] buffer;
        private int count;
        private readonly object _lock = new object();

        private readonly ManualResetEvent cons = new ManualResetEvent(true);
        private readonly ManualResetEvent prod = new ManualResetEvent(true);

        public EventBoundedQueue(int size)
        {
            buffer = new T[size];
        }

        public void Add(T e)
        {
            for (;;)
            {
                prod.WaitOne();
                lock (_lock)
                {
                    if (count == buffer.Length)
                    {
                        prod.Reset();
                        continue;
                    }
                    buffer[count] = e;
                    ++count;
                    if (count == 1)
                        cons.Set();
                    return;
                }
            }
        }
        
        public T Remove()
        {
            for (;;)
            {
                cons.WaitOne();
                lock(_lock)
                {
                    if (count == 0)
                    {
                        cons.Reset();
                        continue;
                    }
                    --count;
                    if (count == buffer.Length - 1)
                        prod.Set();
                    return buffer[count];
                }
            }

        }
    }
}
