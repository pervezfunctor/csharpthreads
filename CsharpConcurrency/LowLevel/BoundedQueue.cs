﻿namespace Seartipy
{
    public class BoundedQueue<T>
    {
        private readonly Lock _lock = new Lock();
        private readonly T[] buffer;
        private int count;

        public BoundedQueue(int size)
        {
            buffer = new T[size];
        }

        public void Add(T e)
        {
            var lockTaken = false;
            try
            {
                _lock.Acquire(ref lockTaken);
                var cond = _lock.GetCondition();
                while (count >= buffer.Length) 
                    cond.Wait();
                buffer[count] = e;
                ++count;
                if(count == 1)
                    cond.NotifyAll();
            }
            finally
            {
                if(lockTaken)
                    _lock.Release();
            }
        }
        
        public T Remove()
        {
            var lockTaken = false;
            try
            {
                _lock.Acquire(ref lockTaken);
                var cond = _lock.GetCondition();
                while (count <= 0)
                    cond.Wait();
                --count;
                var result = buffer[count];
                if(count == buffer.Length - 1)
                    cond.NotifyAll();
                return result;
            }
            finally
            {
                if(lockTaken)
                    _lock.Release();
            }
        }
    }
}
