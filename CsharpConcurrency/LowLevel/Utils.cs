﻿using System;
using System.Collections.Generic;

namespace Seartipy
{
    public static class Utils
    {
        public static IEnumerable<int> To(this int start, int stop, int step = 1)
        {
            for (; start < stop; start += step)
                yield return start;
        }

        public static IEnumerable<long> To(this long start, long stop, long step = 1)
        {
            for (; start < stop; start += step)
                yield return start;
        }

        public static void Each<T>(this IEnumerable<T> list, Action<T> action)
        {
            foreach (var e in list)
                action(e);
        }
    }
}