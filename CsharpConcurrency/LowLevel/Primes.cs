﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Seartipy
{
    public class Primes
    {
        public static bool IsPrime(int n)
        {
            return 2.To(n).FirstOrDefault(i => n%i == 0) == 0;
        }

        public static List<int> All(int start, int stop)
        {
            return start.To(stop).Where(IsPrime).ToList();
        }

        public static List<int> AllParallel(int start, int stop)
        {
            const int THREAD_COUNT = 8;
            var list = new List<int>[THREAD_COUNT];

            Func<Tuple<int, int>, int, Thread> craetePrimesThread = 
                (range, i) => new Thread(() => list[i] = All(range.Item1, range.Item2));
            
            var threads = 
                Partition.Ranges(start, stop, THREAD_COUNT)
                .Select(craetePrimesThread)
                .ToArray();

            Debug.Assert(threads.Length == THREAD_COUNT);
            threads.Each(t => t.Start());
            threads.Each(t => t.Join());

            return list.SelectMany(e => e).ToList();
        }

        static void Main()
        {
            const int N = 1000000;
            var primes = AllParallel(1, N);
            var primes2 = All(1, N);
            Debug.Assert(primes.Count == primes2.Count);
            for(var i = 0; i < primes.Count; ++i)
                Debug.Assert(primes[i] == primes2[i]);
        }
    }
}
