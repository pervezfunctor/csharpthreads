﻿using System;
using System.Threading;

namespace Seartipy
{
    public class Counter
    {
        private int count = 1;
        private readonly object mutex = new object();

        public int Count
        {
            get { return count; }
        }

        public void Incrementer()
        {
            for (var i = 0; i < 100000000; ++i)
            {
                lock (mutex)
                {
                    ++count;
                }
            }
        }

        public void Decrementer()
        {
            for (var i = 0; i < 100000000; ++i)
            {
                lock (mutex)
                {
                    --count;
                }
            }
        }

        static void Main()
        {
            var counter = new Counter();
            var incThread = new Thread(counter.Incrementer);
            var decThread = new Thread(counter.Decrementer);

            incThread.Start();
            decThread.Start();

            incThread.Join();
            decThread.Join();

            Console.WriteLine(counter.Count);
        }
    }
}