﻿using System.Threading;
using System.Diagnostics;

namespace Seartipy
{
    class ReaderWriter
    {
        private readonly object readerLock = new object();
        private readonly object writerLock = new object();
        private int readers;

        public void ReadLock()
        {
            Monitor.Enter(readerLock);
            ++readers;
            if (readers == 1)
                Monitor.Enter(writerLock);
            Monitor.Exit(readerLock);
        }

        public void WriteLock()
        {
            lock (readerLock)
            {
                Debug.Assert(readers == 0);
            }
            Monitor.Enter(writerLock);
        }

        public void ReadUnLock()
        {
            Monitor.Enter(readerLock);
            if(--readers == 0)
                Monitor.Exit(writerLock);
            Monitor.Exit(readerLock);
        }

        public void WriteUnlock()
        {
            Monitor.Exit(writerLock);
        }
    }
}


