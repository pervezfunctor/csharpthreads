using System;
using System.Collections.Generic;
using System.Linq;

namespace Seartipy
{
    public static class Partition
    {
        public static IEnumerable<Tuple<int, int>> Ranges(int start, int stop, int n)
        {
            var currentStart = start;
            foreach (var p in Parts(stop - start, n))
            {
                yield return Tuple.Create(currentStart, currentStart + p);
                currentStart += p;
            }
        }

        private static IEnumerable<int> Parts(int length, int n)
        {
            var part = (int) Math.Round((double) length/n);
            var extra = Math.Abs(length - n*part);
            var sign = Math.Sign(length - n*part);
            return Enumerable.Repeat(part, n - extra).Concat(Enumerable.Repeat(part + sign, extra));
        }
    }
}
