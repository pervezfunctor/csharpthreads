﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Seartipy.Tasks
{
    interface IGenerator<out T>
    {
        T Next();
    }

    class Decrementer : IGenerator<int>
    {
        private int current;
        private readonly object _lock = new object();
        public Decrementer (int from)
	    {
            this.current = from;
	    }

        public int Next()
        {
            lock(_lock)
                return --current;
        }
    }

    class Tasks
    {
        public static int NextPrime(int from)
        {
            for (var i = from + 1; ; ++i)
                if (Primes.IsPrime(i))
                    return i;
        }

        public static void ActionTaskExample()
        {
            Task task = Task.Run(() =>
            {
                var primeNo = NextPrime(1000);
                Console.WriteLine(primeNo);
            });
            task.Wait();
        }

        public static void TaskExample()
        {
            Task<int> task = Task.Run(() => NextPrime(1000));
            Console.WriteLine(task.Result);
        }

        public static void ExceptionExample()
        {
            Task task = Task.Run(() => { throw new Exception("Task Exception occured"); });
            try
            {
                task.Wait();
            }
            catch(AggregateException ex)
            {
                Console.WriteLine(ex.InnerExceptions[0].Message);
            }
        }

        public static void MultipleExceptionsExample()
        {
            Task outerTask = Task.Run(() =>
            {
                Task first = Task.Run(() => { throw new Exception("first Task Exception occured"); });
                Task second = Task.Run(() => { throw new Exception("second Task Exception occured"); });
                Task third = Task.Run(() => { throw new Exception("third Task Exception occured"); });
                Task.WaitAll(first, second, third);
            });

            try
            {
                outerTask.Wait();
            }
            catch (AggregateException ex)
            {
                ex.Handle(e =>
                {
                    if (e is OperationCanceledException)
                    {
                        Console.WriteLine("cancelled!");
                        return true;
                    }
                    return false;
                });
                Console.WriteLine(ex.Flatten().InnerExceptions.Count);
            }
        }

        public static int NextPrime(IGenerator<int> gen, CancellationToken token)
        {
            var i = gen.Next();
            while(!token.IsCancellationRequested)
            {
                if (Primes.IsPrime(i))
                    return i;
                i = gen.Next();
            }
            return -1;
        }        

        public static Task<int> LargePrime(int lessThan)
        {
            var dec = new Decrementer(lessThan);
            var cts = new CancellationTokenSource();
            var token = cts.Token;

            var first = Task.Run(() => NextPrime(dec, token), token);
            var second = Task.Run(() => NextPrime(dec, token), token);
            var third = Task.Run(() => NextPrime(dec, token), token);

            return Task.WhenAny(first, second, third).ContinueWith(t =>
            {
                cts.Cancel();
                return t.Result.Result;
            }, token);
        }

        static void Main()
        {
            Console.WriteLine(LargePrime(int.MaxValue - 1000).Result);
            Console.ReadKey();
        }
    }
}

