﻿using System;
using System.Threading;

namespace Seartipy
{
   public class AutoResetEventExample
    {
        private static readonly AutoResetEvent Simple = new AutoResetEvent(false);
        
        public static void Waiter()
        {
            Console.WriteLine("I am waiting");
            Simple.WaitOne();
            Console.WriteLine("Wait over!!!");
        }

        public static void Sender()
        {
            Console.WriteLine("Setting event");
            Simple.Set();
        }

        private const int Iterations = 10;
        private readonly static AutoResetEvent even = new AutoResetEvent(false);
        private readonly static AutoResetEvent odd = new AutoResetEvent(false);

        public static void Even()
        {
            for (var i = 0; i < Iterations; ++i)
            {
                even.WaitOne();
                Console.Write( 2 * i + " ");
                odd.Set();
            }
        }

        public static void Odd()
        {
            for (var i = 0; i < Iterations; ++i)
            {
                even.Set();
                odd.WaitOne();
                Console.Write((2*i + 1) + " ");
            }
        }

        static void Main()
        {
            new Thread(Waiter).Start();
            new Thread(Sender).Start();
            new Thread(Even).Start();
            new Thread(Odd).Start();
        }
    }
}
