﻿using System;
using System.Threading;

namespace Seartipy
{
    public class ProducerConsumer
    {
        private static readonly long ExpectedResult;
        private static readonly BoundedQueue<long> Q = new BoundedQueue<long>(1000);

        private const long Iterations = 10000000;

        public static void Producer()
        {
            for(long i = 0; i < Iterations; ++i)
                Q.Add(i);
        }

        public static void Consumer()
        {
            long sum = 0;
            for (long i = 0; i < Iterations; ++i)
                sum += Q.Remove();
            Console.WriteLine(sum);
        }

        static ProducerConsumer()
        {
            for (long i = 0; i < Iterations; ++i)
                ExpectedResult += i;
        }
        
        static void Main()
        {
            Console.WriteLine(ExpectedResult);
            new Thread(Producer).Start();
            new Thread(Consumer).Start();
        }
    }
}
