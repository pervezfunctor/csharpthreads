﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Seartipy.Tasks
{
    class Pascal
    {
        public static int Factorial(int n)
        {
            var fact = 1;
            for(var i = 2; i <= n; ++i)
                fact *= i;
            return fact;
         }

        public static Task<int> Ncr(int n, int r)
        {
            return Task.WhenAll(
                Task.Run(() => Factorial(n)),
                Task.Run(() => Factorial(r)),
                Task.Run(() => Factorial(n - r))
            ).ContinueWith(
                t => t.Result[0] / (t.Result[1] * t.Result[2])
            );
        }

        public static Task<int[]> Line(int line)
        {
            var tasks = new Task<int>[line + 1];
            foreach (var i in Enumerable.Range(0, line + 1))
                tasks[i] = Ncr(line, i);
            return Task.WhenAll(tasks);
        }

        public static Task<int[][]> Triangle(int n)
        {
            var tasks = new Task<int[]>[n + 1];
            foreach(var i in Enumerable.Range(0, n + 1))
                tasks[i] = Line(i);
            return Task.WhenAll(tasks);
        }

        static void Main()
        {
            Triangle(5).ContinueWith(t =>
            {
                foreach (var line in t.Result)
                {
                    foreach (var e in line)
                        Console.Write("{0}  ", e);
                    Console.WriteLine();
                }
                Console.ReadKey();
            }).Wait();
        }
    }
}
